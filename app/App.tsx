import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import { tabScreenOptions, tabBarOptions } from './app/settings/navigation';
import { BookmarkScreen, SettingsScreen } from './app/screens';
import { SearchStackNavigator } from './app/navigators';

const Tab = createBottomTabNavigator();

export default function App() {
	return (
		<NavigationContainer>
			<Tab.Navigator
				screenOptions={tabScreenOptions}
				tabBarOptions={tabBarOptions}
			>
				<Tab.Screen name='Search' component={SearchStackNavigator} />
				<Tab.Screen name='Bookmark' component={BookmarkScreen} />
				<Tab.Screen name='Settings' component={SettingsScreen} />
			</Tab.Navigator>
		</NavigationContainer>
	);
}
