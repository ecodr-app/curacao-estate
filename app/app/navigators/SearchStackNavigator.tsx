import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { FilterScreen, SearchScreen } from '../screens';

const Stack = createStackNavigator();

const SearchStackNavigator: React.FC = () => {
	return (
		<Stack.Navigator>
			<Stack.Screen name='Search' component={SearchScreen} />
			<Stack.Screen name='Filter' component={FilterScreen} />
		</Stack.Navigator>
	);
};

export default SearchStackNavigator;
