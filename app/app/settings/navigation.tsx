import React from 'react';
import { Feather } from '@expo/vector-icons';
import {
	BottomTabNavigationOptions,
	BottomTabBarOptions,
} from '@react-navigation/bottom-tabs';
import { RouteProp } from '@react-navigation/native';

interface ITabScreenOptions {
	route: RouteProp<Record<string, object | undefined>, string> | any;
	navigation: any;
}

export const tabScreenOptions = ({
	route,
}: ITabScreenOptions): BottomTabNavigationOptions => {
	return {
		tabBarIcon: ({ color, size }) => {
			let iconName: string;

			switch (route.name) {
				case 'Search':
					iconName = 'search';
					break;
				case 'Bookmark':
					iconName = 'bookmark';
					break;
				case 'Settings':
					iconName = 'settings';
					break;
				default:
					iconName = 'home';
			}

			return <Feather name={iconName} size={size} color={color} />;
		},
		tabBarVisible: route?.state?.index > 0 ? false : true,
	};
};

export const tabBarOptions: BottomTabBarOptions = {};
