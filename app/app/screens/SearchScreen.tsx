import React, { useLayoutEffect, useEffect } from 'react';
import { View, StyleSheet, Button } from 'react-native';

const SearchScreen: React.FC<any> = ({ navigation }) => {
	useLayoutEffect(() => {
		navigation.setOptions({
			headerRight: () => (
				<Button title='Filter' onPress={() => navigation.navigate('Filter')} />
			),
			headerLeft: () => (
				<Button title='Alert' onPress={() => navigation.navigate('Filter')} />
			),
		});
	}, [navigation]);

	return <View style={styles.container}></View>;
};

const styles = StyleSheet.create({
	container: {},
});

export default SearchScreen;
